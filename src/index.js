import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import * as firebase from "firebase/app";

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
import "firebase/analytics";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/database";

import { BrowserRouter as Router } from 'react-router-dom';

const firebaseConfig = {
    apiKey: "AIzaSyAwrmcdg7CyHvnNE6nigPtDJ1gJzisidAg",
    authDomain: "personal-finance-df0c1.firebaseapp.com",
    databaseURL: "https://personal-finance-df0c1.firebaseio.com",
    projectId: "personal-finance-df0c1",
    storageBucket: "personal-finance-df0c1.appspot.com",
    messagingSenderId: "426601346682",
    appId: "1:426601346682:web:787f77a354fe73c3a7edc3",
    measurementId: "G-7DWYN4X77Y"
  };

  // Initialize Firebase
firebase.initializeApp(firebaseConfig);

ReactDOM.render(
    <Router>
        <App />
    </Router>,
    document.getElementById('root')
);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
