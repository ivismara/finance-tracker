import React from 'react';
import { Route } from 'react-router-dom';
import { Sidebar } from './components/Sidebar';
import { Savings } from './containers/Savings';
import { Settings } from './containers/Settings';
import { Overview } from './containers/Overview';
import { Login } from './containers/Login';
import { Redirect } from 'react-router-dom';
import { Layout, Button, notification, Icon } from 'antd';
import moment from 'moment';
import firebase from 'firebase'

export class Wrapper extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            accountList: [],
            savings: [
                { id: '21323', potName: 'New Trip', savedMoney: 23123 },
                { id: '3212', potName: 'Lamborghini', savedMoney: 3232 },
            ],
            categories: [],
            transactions: [],
            logged: "",
            firstAccountNeeded: false,
            selectedAccount: 'overview',
            isMobileSidebarVisible: false
        };
        this.addNewAccount = this.addNewAccount.bind(this);
        this.addTransaction = this.addTransaction.bind(this);
        this.showSelectedAccount = this.showSelectedAccount.bind(this);
        this.logout = this.logout.bind(this);

    }

    addNewAccount(e, newAccount) {
        if (typeof newAccount.salary === 'undefined' || typeof newAccount.payday === 'undefined') {
            newAccount.salary = null;
            newAccount.payday = null;
        }
        const accountListRef = firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/accountList/');
        let newAccountListRef = accountListRef.push();
        newAccountListRef.set({
            name: newAccount.name,
            balance: newAccount.balance,
            salary: newAccount.salary,
            payDay: newAccount.payday,
        })

        newAccount.id = newAccountListRef.key;

        this.setState(prevState => ({
            accountList: [...prevState.accountList, newAccount],
            firstAccountNeeded: false
        }));

        notification.open({
            message: 'Account Added!',
            description:
                'Congrats! You\'ve added a new account!',
            icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
        });
    }

    addTransaction(e, newTransaction) {

        const fixedTransaction = new Object();

        //trasforma valore in base a outcome/income

        fixedTransaction.description = newTransaction.description;
        fixedTransaction.date = newTransaction.date.format('MMMM DD YYYY');
        fixedTransaction.category = newTransaction.category;
        if (newTransaction.source === 'outcome') {
            fixedTransaction.amount = - newTransaction.amount;
        }else{
            fixedTransaction.amount = newTransaction.amount
        }
        fixedTransaction.account = newTransaction.account;
        const transactionsListRef = firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/transactions/');
        let newTransactionsListRef = transactionsListRef.push();
        newTransactionsListRef.set({
            description: fixedTransaction.description,
            date: fixedTransaction.date,
            category: fixedTransaction.category,
            amount: fixedTransaction.amount,
            account: fixedTransaction.account
        })

        fixedTransaction.id = newTransactionsListRef.key;


        //aggiornamento balance
        let accountListCopy = this.state.accountList;
        const accountIndex = accountListCopy.findIndex(x => x.id === fixedTransaction.account);

        const newBalance = accountListCopy[accountIndex].balance + fixedTransaction.amount

        accountListCopy[accountIndex].balance = newBalance;

        //on firebase

        const newBalanceForFirebase = { balance: newBalance };
        const accountToBeUpdated = firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/accountList/' + fixedTransaction.account);
        accountToBeUpdated.update(newBalanceForFirebase)


        //set of state
        this.setState(prevState => ({
            transactions: [...prevState.transactions, fixedTransaction],
            accountList: accountListCopy
        }))

        notification.open({
            message: 'Transaction Added!',
            description:
                'Congrats! You\'ve added a new transaction!',
            icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
        });
    }

    showSelectedAccount(e, accountId) {
        this.setState({
            selectedAccount: accountId
        })
    }

    logout() {
        firebase.auth().signOut().then(() => {
            this.setState({
                logged: false
            });
        }).catch(function (error) {
            // An error happened.
        });
    }

    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            if (user) {

                const userId = firebase.auth().currentUser.uid;
                return firebase.database().ref('/users/' + userId).once('value').then(snapshot => {

                    //retrieve accountlist
                    let accountList = (snapshot.val().accountList);
                    if (typeof accountList === 'undefined') {
                        this.setState({
                            firstAccountNeeded: true
                        })
                    } else {

                        let accountListArray = [];

                        //convert firebase object into an array of objects for accounts 
                        Object.keys(accountList).map(key => {
                            let account = new Object();
                            account.id = key;
                            account.name = accountList[key].name;
                            account.payDay = accountList[key].payday;
                            account.salary = accountList[key].salary;
                            account.balance = accountList[key].balance;

                            accountListArray.push(account);
                        })

                        this.setState({
                            accountList: accountListArray
                        });
                    }

                    //retrieve categories
                    let categories = (snapshot.val().categories);

                    if (typeof categories !== 'undefined') {
                        let categoriesArray = [];

                        //convert firebase object in an array of objects for categories
                        Object.keys(categories).map(key => {
                            let category = new Object();
                            category.id = key;
                            category.name = categories[key].name;
                            category.color = categories[key].color;
                            category.icon = categories[key].icon;
                            category.type = categories[key].type;

                            categoriesArray.push(category);
                        })

                        categoriesArray.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));

                        this.setState({
                            categories: categoriesArray
                        })
                    }

                    //retrieve transactions
                    let transactions = (snapshot.val().transactions);
                    if (typeof transactions !== 'undefined') {
                        let transactionsArray = [];

                        //convert firebase object in an array of objects for transactions
                        Object.keys(transactions).map(key => {
                            let transaction = new Object();
                            transaction.id = key;
                            transaction.description = transactions[key].description;
                            transaction.date = transactions[key].date;
                            transaction.amount = transactions[key].amount;
                            transaction.category = transactions[key].category;
                            transaction.account = transactions[key].account;

                            transactionsArray.push(transaction);
                        });

                        const sortedTransactionArray = transactionsArray.sort((a, b) => new moment(b.date).format('YYYYMMDD') - new moment(a.date).format('YYYYMMDD'));

                        this.setState({
                            transactions: sortedTransactionArray
                        })
                    };

                    let userCreationDate  = (snapshot.val().signUpDate)

                    this.setState({
                        userCreationDate : userCreationDate
                    })


                });
            } else {
                this.logout()
            }
        });
    }

    handleUpdateTransaction = (id, updatedTransaction) => {
        //transaction Update
        firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/transactions/' + id).set({
            'account': updatedTransaction.account,
            'amount': updatedTransaction.amount,
            'category': updatedTransaction.category,
            'date': updatedTransaction.date.format('MMMM DD YYYY'),
            'description': updatedTransaction.description
        })

        const updatedTransactionForLocalArray = updatedTransaction

        updatedTransactionForLocalArray.id = id;
        updatedTransactionForLocalArray.date = updatedTransaction.date.format('MMMM DD YYYY')
        const updatedTransactionList = [...this.state.transactions];
        const indexToBeUpdated = updatedTransactionList.findIndex(x => x.id === id);

        //account Update
        let accountListCopy = [...this.state.accountList];
        const oldTransactionAmount = updatedTransactionList[indexToBeUpdated].amount;
        const accountIndex = accountListCopy.findIndex(x => x.id === updatedTransactionForLocalArray.account);
        const newBalance = accountListCopy[accountIndex].balance + updatedTransactionForLocalArray.amount - oldTransactionAmount

        //replace transaction
        updatedTransactionList[indexToBeUpdated] = updatedTransactionForLocalArray;
        accountListCopy[accountIndex].balance = newBalance;

        //on firebase

        const newBalanceForFirebase = { balance: newBalance };
        const accountToBeUpdated = firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/accountList/' + updatedTransactionForLocalArray.account);
        accountToBeUpdated.update(newBalanceForFirebase)


        this.setState({
            transactions: updatedTransactionList,
            accountList: accountListCopy
        })

        notification.open({
            message: 'Transaction Edited!',
            description:
                'Selected transaction had been edited!',
            icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
        });
    }

    handleDeleteTransaction = (id, account, amount) => {
        //transaction remove
        firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/transactions/' + id).remove();

        //account update
        let accountListCopy = [...this.state.accountList];
        const accountIndex = accountListCopy.findIndex(x => x.id === account);
        const newBalance = accountListCopy[accountIndex].balance - amount;
        accountListCopy[accountIndex].balance = newBalance;


        this.setState({
            transactions: this.state.transactions.filter(transaction => transaction.id !== id),
            accountList: accountListCopy
        })

        notification.open({
            message: 'Transaction Removed!',
            description:
                'The selected transaction has been removed!',
            icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
        });
    }

    showSidebarMobile = () => {
        this.setState({
            isMobileSidebarVisible: true
        })
    }

    handleCloseMobileSidebar = () => {
        this.setState({
            isMobileSidebarVisible: false
        })
    }

    render() {

        const { Header, Content } = Layout;

        if (this.state.logged === false) {
            return <Redirect to='/login' />
        }
        return (
            <div>
                <Layout>
                    <Route render={() => <Sidebar  selectedAccount={this.state.selectedAccount} isMobileSidebarVisible={this.state.isMobileSidebarVisible} closeMobileSidebar={() => this.handleCloseMobileSidebar()} logout={() => this.logout()} firstAccountNeeded={this.state.firstAccountNeeded} accountList={this.state.accountList} showSelectedPage={(e, accountId) => this.showSelectedAccount(e, accountId)} addNewAccount={(e, newElement) => this.addNewAccount(e, newElement)} />} path="/" />
                    <Layout className="main-container">
                        <Header className="mobile-header"><Button className="mobile-sidebar-toggler" type="link" onClick={this.showSidebarMobile}><span className="uil uil-bars"></span></Button></Header>
                      
                            <Route path="/" exact render={() => <Overview userCreationDate={this.state.userCreationDate} handleDeleteTransaction={(id, account, amount) => this.handleDeleteTransaction(id, account, amount)} handleUpdateTransaction={(id, updatedTransaction) => this.handleUpdateTransaction(id, updatedTransaction)} selectedAccount={this.state.selectedAccount} transactions={this.state.transactions} addTransaction={(e, newTransaction) => this.addTransaction(e, newTransaction)} categories={this.state.categories} accountList={this.state.accountList} />} />
                            <Route path="/savings" exact component={Savings} />
                            <Route path="/settings" exact component={Settings} />
                          
                        <Route path="/login" exact component={Login} />
                        
                    </Layout>
                </Layout>
            </div>
        );
    }
}

export default Wrapper;