import React from "react";
import { Route, Switch } from "react-router-dom";
import Wrapper from "./Wrapper";
import NotFound from "./containers/NotFound";
import Login from "./containers/Login";

export default function Routes() {
    return (
        <Switch>
            <Route path="/login" exact component={Login} />
            <Route component={Wrapper}/>
            { /* Finally, catch all unmatched routes */}
            <Route component={NotFound} />
        </Switch>
    );
}