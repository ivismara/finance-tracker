import React from 'react';
import { Typography, Button, Popover, Modal } from 'antd';
import { TransactionCard } from './TransactionCard';
import moment from 'moment';
import { AddTransactionDrawer } from './AddTransactionDrawer'


export class LastTransactionsList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            displayingElements: 5,
        }
    }

    handleShowMoreClick = () => {
        this.setState((prevState) => ({
            displayingElements: prevState.displayingElements + 10
        }))
    }

    MonthTitle = props => {
        if ((new Date(props.currentTransactionDate).getMonth() + 1).toString() !== (new Date(props.prevTransactionDate).getMonth() + 1).toString() || (new Date(props.currentTransactionDate).getFullYear()).toString() !== (new Date(props.prevTransactionDate).getFullYear()).toString() || props.currentTransactionIndex === 0) {
            const { Title } = Typography;
            return <Title style={{ textAlign: 'right', paddingRight: 30, paddingTop: 20, paddingBottom: 20 }} level={4}>{moment(props.currentTransactionDate).format('MMMM YYYY')}</Title>
        } else {
            return null
        }
    }

    ShowMoreButton = props => {
        if (props.isLastTransactionOfGroup && !(props.transactionLimit === (props.currentTransactionIndex + 1))) {
            return <Button onClick={this.handleShowMoreClick} style={{ textAlign: 'center', width: '100%', color: '#ffd32a' }} type="link">Show more</Button>
        } else {
            return null
        }
    }

    handleUpdateTransaction = (id, updatedTransaction) => {
        this.props.handleUpdateTransaction(id, updatedTransaction)
    }

    handleDeleteTransaction = (id, account, amount) => {
        this.props.handleDeleteTransaction(id, account, amount)
    }


    handleTransactionAdded = (e, newTransaction) => {
        this.props.handleTransactionAdded(e, newTransaction)
    }

    retrieveOverviewValues = () => {
        this.props.retrieveOverviewValues()
    }
 


    render() {
        let transactionLimit = this.props.filteredTransactions.length;
        return (
            <div>
                <div style={{ display: 'flex', justifyContent: 'space-between', paddingTop: 20, alignItems: 'center', flexWrap: 'wrap' }}>
                    <AddTransactionDrawer selectedAccount={this.props.selectedAccount} formatToEur={(e) => this.props.formatToEur(e)} handleTransactionAdded={(e, newTransaction) => this.handleTransactionAdded(e, newTransaction)} retrieveOverviewValues={() => this.retrieveOverviewValues()} accountList={this.props.accountList} categories={this.props.categories} />
                </div>
                {this.props.filteredTransactions.slice(0, this.state.displayingElements).map((transaction, index, array) => {
                    let prevTransaction = new Object(array[index - 1]);
                    let prevTransactionDate = new moment(prevTransaction.date).format('YYYY-MM-DD')
                    let currentTransactionDate = new moment(transaction.date).format('YYYY-MM-DD')

                    let isLastTransactionOfGroup = index === (array.length - 1) ? true : false;

                    return (
                        <div key={index}>
                            <this.MonthTitle prevTransactionDate={prevTransactionDate} currentTransactionDate={currentTransactionDate} currentTransactionIndex={index} />
                            <TransactionCard formatToEur={(e) => this.props.formatToEur(e)} handleDeleteTransaction={(id, account, amount) => this.handleDeleteTransaction(id, account, amount)} handleUpdateTransaction={(id, updatedTransaction) => this.handleUpdateTransaction(id, updatedTransaction)} accountList={this.props.accountList} key={transaction.id} categories={this.props.categories} transaction={transaction} />
                            <this.ShowMoreButton isLastTransactionOfGroup={isLastTransactionOfGroup} transactionLimit={transactionLimit} currentTransactionIndex={index} />
                        </div>
                    )
                })}
            </div>
        )
    };
}