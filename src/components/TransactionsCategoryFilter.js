import React from 'react';

import { Checkbox, Popover, Button, Modal } from 'antd';
import Div100vh from 'react-div-100vh'


export class TransactionsCategoryFilter extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isCategoryFilterMobileModalVisible: false,
            filteredItems: []
        }
    }

    onChange = checkedList => {
        this.setState({
            filteredItems: checkedList
        }, () => {
            this.props.updateCategoryFilter(this.state.filteredItems);
        })
    }

    addCategoryToFilteredItems = category => {
        this.setState(prevState => ({
            filteredItems: [...prevState.filteredItems, category],
        }))
    }

    showMobileModalCategoryFilter = () => {
        this.setState({
            isCategoryFilterMobileModalVisible: true
        })
    }

    handleRemoveCategoryFilter = () => {
        this.props.handleRemoveCategoryFilter();
        this.hideMobileModalCategoryFilter()
    }

    hideMobileModalCategoryFilter = () => {
        this.setState({
            isCategoryFilterMobileModalVisible: false
        });
    };

    addCategoryToFilters = category => {
        this.props.addCategoryToFilters(category)
    }

    removeCategoryFromFilters = category => {
        this.props.removeCategoryFromFilters(category)
    }

    
    ClearCategoryFilter = () => {
        if (this.props.isCategoryFilterActive) {
            return <Button type="default" onClick={this.handleRemoveCategoryFilter} className="clear-filters-btn secondary-custom-btn"><i className="uil uil-times"></i></Button>
        } else {
            return null
        }
    }

    ClearCategoryFilterMobile = () => {
        if (this.props.isCategoryFilterActive) {
            return <Button size="small" type="default" onClick={this.handleRemoveCategoryFilter} className="clear-filters-btn mobile-filter-btn secondary-custom-btn"><i className="uil uil-times"></i></Button>
        } else {
            return null
        }
    }


    render() {


        return (
            window.matchMedia("(max-width: 1199.98px)").matches ? (
                <div className="category-filter-mobile-btn-container">
                        {this.props.checkBoxStartingCategories.map(category => {
                            console.log(this.props.checkBoxStartingCategories)
                                return (<Button
                                    onClick={this.props.filteredCategories.indexOf(category.value) > -1 ? ()=> this.removeCategoryFromFilters(category.value) : () => this.addCategoryToFilters(category.value)}
                                    size="small"
                                    className="mobile-filter-btn"
                                    style={{ backgroundColor: category.color, opacity: this.props.filteredCategories.indexOf(category.value) > -1 ? 1 : 0.4 }} 
                                    key={category.value}>
                                        {category.label}
                                </Button>
                                )
                            })
                        }
                         <this.ClearCategoryFilterMobile />
                         <div style={{minWidth: 20, height: 24}}></div>
                </div>
            ) : (
                <div>
                    <Popover
                        getPopupContainer={trigger => trigger.parentNode}
                        title="Filter by Category"
                        placement="topRight"
                        trigger="click"
                        content={<Checkbox.Group onChange={this.onChange} value={this.props.filteredCategories} options={this.props.checkBoxStartingCategories} />
                        }>
                        <Button className="filter-btn" type={this.props.isCategoryFilterActive ? 'primary' : 'link'}><i style={{ fontSize: 18 }} className='uil uil-filter'></i></Button>
                    </Popover>
                    <this.ClearCategoryFilter />
                        </div>
                )
        )
    }
}