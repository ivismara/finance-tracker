import React from 'react';
import { Form, Input, Button, Select, Radio, Popover } from 'antd';

import CurrencyInput from 'react-currency-input';
import moment from 'moment';
import { CustomSingleDatePicker } from './CustomSingleDatePicker';

class NewTransactionForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            amount: 0.00,
            date: new moment(),
            isAmountValid: 'notset'
        }

        this.transactionTypeSwitch = this.transactionTypeSwitch.bind(this)
    }

    handleSubmit = e => {
        e.preventDefault();
        if (this.state.isAmountValid === true) {
           
            this.props.form.validateFields((err, values) => {
                if (!err) {
                    this.props.handleTransactionAdded(e, values);
                } else {
                    console.log(values)
                }
            });
            this.props.form.resetFields();
            this.setState({
                amount: 0.00,
                date: new moment()
            })
        } else {
            this.setState({
                isAmountValid: false
            })
        }
    };

    handleAmountChange = (event, maskedvalue, floatvalue) => {
        this.props.form.setFieldsValue({
            amount: floatvalue,
        });

        if (floatvalue === 0) {
            this.setState({
                isAmountValid: false,
                amount: floatvalue,
            })
        } else {
            this.setState({
                amount: floatvalue,
                isAmountValid: true
            });
        }

    }

    handleDateChange = date => {
        this.props.form.setFieldsValue({
            date: date,
        });
        this.setState({
            date: date,
        })
    }

    transactionTypeSwitch = type => {

     this.setState({
            transactionCategoryType: type.target.value
        }, () => {
            console.log(this.state.transactionCategoryType)
        })
    }

    componentDidMount = () => {
        this.setState({
            transactionCategoryType: 'outcome'
        })
    }

    render() {

        let componentDidMount_super = CurrencyInput.prototype.componentDidMount; CurrencyInput.prototype.componentDidMount = function() { this.theInput.setSelectionRange_super = this.theInput.setSelectionRange; this.theInput.setSelectionRange = (start, end) => { if (document.activeElement === this.theInput) { this.theInput.setSelectionRange_super(start, end); } }; componentDidMount_super.call(this, ...arguments); };

        const { getFieldDecorator } = this.props.form;
        const { Option } = Select;
        return (
            <Form onSubmit={this.handleSubmit}>

                <Form.Item style={{ display: 'flex', justifyContent: 'center' }}>
                    {getFieldDecorator('source', {
                        rules: [{ required: true }],
                        initialValue: 'outcome'
                    })(
                        <Radio.Group onChange={this.transactionTypeSwitch} buttonStyle="solid">
                            <Radio.Button value="outcome">Outcome</Radio.Button>
                            <Radio.Button value="income">Income</Radio.Button>
                        </Radio.Group>)}
                </Form.Item>
                <Form.Item label="Description">
                    {getFieldDecorator('description', {
                        rules: [{ required: true, message: 'Please input a short description (eg. Netflix renewal, Salary...)' }],
                    })(
                        <Input
                            placeholder="eg.Train Ticket"
                        />,
                    )}
                </Form.Item>

                <Form.Item label="Date">
                    {getFieldDecorator('date', {
                        rules: [{ required: true, message: 'Please input a valid date' }],
                        initialValue: this.state.date
                    })(
                        <div>
                            <Input
                                style={{ display: 'none' }}
                            />
                            <Popover
                                overlayClassName="date-edit"
                                content={<CustomSingleDatePicker handleDateChange={date => this.handleDateChange(date)} />}
                                trigger="click"
                                placement="bottomLeft"
                            >
                                <Button style={{ width: '100%', textAlign: 'left', lineHeight: '100%' }} className="ant-form-item-control">{moment(this.state.date).format('MMMM DD YYYY')}</Button>
                            </Popover>
                        </div>
                    )}
                </Form.Item>

                <Form.Item label="Category">
                    {getFieldDecorator('category', {
                        rules: [{ required: true, message: 'Please select a category' }],
                    })(
                        <Select
                            showSearch
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                        >
                            {this.props.categories.filter(category => category.type === this.state.transactionCategoryType).map((category) => (
                                <Option style={{ borderLeft: '3px solid', borderLeftColor: category.color }} key={category.id}>{category.name}</Option>
                            ))}
                        </Select>
                    )}
                </Form.Item>

                <Form.Item label="Account">
                    {getFieldDecorator('account', {
                        rules: [{ required: true, message: 'Please select an account' }],
                        initialValue: this.props.selectedAccount === 'overview' ? this.props.accountList[0].id : this.props.selectedAccount
                    })(
                        <Select>
                            {/*da sostituire con eventuale id*/}
                            {this.props.accountList.map((account, index) => (
                                <Option value={account.id} key={index}>{account.name}</Option>
                            ))}
                        </Select>
                    )}
                </Form.Item>
                <Form.Item help={this.state.isAmountValid ? null : "Amount can't be 0!"} validateStatus={this.state.isAmountValid ? null : "error"} label="Amount">
                    {getFieldDecorator('amount', {
                        initialValue: this.state.amount
                    })(
                        <div>
                            <Input precision={2} style={{ width: '100%', display: 'none' }} />
                            <CurrencyInput autoFocus={false} thousandSeparator=" " decimalSeparator="," suffix="€" className="ant-input-number" style={{ width: '100%', padding: '4px 11px' }} value={this.state.amount} onChangeEvent={this.handleAmountChange} />
                        </div>
                    )}
                </Form.Item>
                <Form.Item style={{ marginBottom: 0, display: 'flex', justifyContent: 'center' }}>

                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Add Transaction!
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

export default Form.create({ name: 'new_transaction' })(NewTransactionForm);