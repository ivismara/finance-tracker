import React from 'react';
import { TransactionDateFilters } from "../components/TransactionDateFilters";
import { TransactionsCategoryFilter } from './TransactionsCategoryFilter';

import { Button } from 'antd'

import './FiltersContainer.css'

export class FiltersContainer extends React.Component {
    constructor(props) {
        super(props)
    }

    handleRemoveDateFilter = () => {
        this.props.handleRemoveDateFilter();
    }

    handleUpdateDateFilter = (startDate, endDate, activeDateFilter) => {
        this.props.handleUpdateDateFilter(startDate, endDate, activeDateFilter)
    }


    handleUpdateCategoryFilter = filteredCategories => {
        this.props.handleUpdateCategoryFilter(filteredCategories)
    }

    handleRemoveCategoryFilter = () => {
        this.props.handleRemoveCategoryFilter();
    }


    addCategoryToFilters = category => {
        if (this.props.filteredCategories.length !== this.props.checkBoxStartingCategories.length) {
            this.props.addCategoryToFilters(category)
        } else {
            this.props.addFirstCategoryToFilters(category)
        }
    }

    render() {
        return (
            <div className="filters-container">
               
                <div className="filter-group filter-group-category">
                    <TransactionsCategoryFilter
                        isCategoryFilterActive={this.props.isCategoryFilterActive}
                        filteredCategories={this.props.checkBoxStartingCategories.length !== this.props.filteredCategories.length ? this.props.filteredCategories : []}
                        updateCategoryFilter={(filteredCategories) => this.handleUpdateCategoryFilter(filteredCategories)}
                        checkBoxStartingCategories={this.props.checkBoxStartingCategories}
                        handleRemoveCategoryFilter={() => this.handleRemoveCategoryFilter()}
                        topCategoriesValues={this.props.topCategoriesValues}
                        addCategoryToFilters={category => this.addCategoryToFilters(category)}
                        removeCategoryFromFilters={category => this.props.removeCategoryFromFilters(category)}
                    />
                  
                </div>
                <div className="filter-group filter-group-date">
                    <TransactionDateFilters
                      userCreationDate={this.props.userCreationDate}
                        isDateFilterActive={this.props.isDateFilterActive}
                        filteredDates={this.props.filteredDates}
                        handleRemoveDateFilter={() => this.handleRemoveDateFilter()}
                        handleUpdateDateFilter={(startDate, endDate, activeDateFilter) => this.handleUpdateDateFilter(startDate, endDate, activeDateFilter)}
                    />
                </div>
            </div>
        )
    }
}