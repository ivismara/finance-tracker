import React from 'react';
import { Layout, Menu, Icon, Button, Drawer } from 'antd';
import { AddAccountModal } from './AddAccountModal';
import { Link } from 'react-router-dom';

import './Sidebar.css';
import { MoneyTransferModal } from './MoneyTransferModal';

export class Sidebar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isMoneyTransferModalVisible: false
        }
        this.handleAccountAdded = this.handleAccountAdded.bind(this);
        this.handleClickMenu = this.handleClickMenu.bind(this);
        this.logout = this.logout.bind(this);
    }

    handleAccountAdded = (e, newElement) => {
        this.props.addNewAccount(e, newElement);
    };

    handleClickMenu = (e, key) => {
        this.props.showSelectedPage(e, key);
    }

    logout = () => {
        this.props.logout();
    }

    closeMobileSidebar = () => {
        this.props.closeMobileSidebar()
    };

    showMoneyTransferModal = () => {
       this.setState({
           isMoneyTransferModalVisible: true
       })
    }
    
    closeMoneyTransferModal = () => {
        this.setState({
            isMoneyTransferModalVisible: false
        })
    }

    SidebarContent = () => {

        const { SubMenu } = Menu;
        return (
            <Menu
                mode="inline"
                defaultSelectedKeys={['overview']}
                defaultOpenKeys={['accounts']}
                style={{ height: '100%', borderRight: 0, display: 'flex', flexDirection: 'column' }}
            >
                <Menu.Item className="sidebar-item" onClick={(e) => this.handleClickMenu(e, 'overview')} key="overview">
                    <Link to="/">
                        <i className="uil uil-chart-pie-alt"></i>
                        <span>Overview</span>
                    </Link>
                </Menu.Item>
                <SubMenu className="sidebar-item"
                    key="accounts"
                    title={
                        <span>
                            <i class='uil uil-wallet'></i>
                            Accounts
        </span>
                    }
                >
                    {this.props.accountList.map(account => (

                        <Menu.Item   onClick={(e) => this.handleClickMenu(e, account.id)}  key={account.id}>

                            <Link to="/">{account.name}</Link>
                            <Button className="money-transfer-icon" onClick={this.showMoneyTransferModal} shape="circle" type="primary"><span className="uil uil-exchange-alt"></span> </Button>

                        </Menu.Item>

                    ))}
                    <div style={{ paddingLeft: 48, paddingRight: 48, marginBottom: 16, paddingTop: 8 }}>
                        <AddAccountModal firstAccountNeeded={this.props.firstAccountNeeded} handleAccountAdded={(e, newElement) => this.handleAccountAdded(e, newElement)} />
                    </div>
                </SubMenu>

                <Menu.Item className="sidebar-item" key="savings">
                    <Link to="/savings">
                        <Icon type="euro" />
                        <span>Savings</span>
                    </Link>
                </Menu.Item>
                <Menu.Item className="sidebar-item" key="settings">
                    <Link to="/settings">
                        <Icon type="setting" />
                        <span>Settings</span>
                    </Link>
                </Menu.Item>
                <Button onClick={this.logout}>Logout</Button>
            </Menu>
        )
    }

    render() {
        const { Sider } = Layout;

        return (
            <div>
                <Sider className="custom-sidebar" width={300}>
                    <this.SidebarContent />
                </Sider>

                <MoneyTransferModal accountList={this.props.accountList}  selectedAccount={this.props.selectedAccount} closeMoneyTransferModal={()=> this.closeMoneyTransferModal()} visible={this.state.isMoneyTransferModalVisible}/>

                <Drawer
                    className="custom-mobile-sidebar"
                    placement="left"
                    closable={false}
                    onClose={this.closeMobileSidebar}
                    visible={this.props.isMobileSidebarVisible}
                >
                    <this.SidebarContent />
                </Drawer>
            </div>
        );
    }
}