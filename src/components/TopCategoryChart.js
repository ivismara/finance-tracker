import React from 'react';

import { Typography} from 'antd'
 
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Cell, Tooltip, ResponsiveContainer } from 'recharts';

import './TopCategoryChart.css';

export class TopCategoryChart extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activeTooltip: false,
        }

        this.onBarClick = this.onBarClick.bind(this)
    }

    customTooltip = e => {
        if (this.state.activeTooltip && e.payload != null && e.payload[0] != null) {
            return (<div className="custom-tooltip">
                <div style={{ color: e.payload[0].payload["color"], fontWeight: 'bold' }} >{e.payload[0].payload["name"]}</div>
                <div style={{ color: '#3B3B3B' }}>{this.props.formatToEur(e.payload[0].payload["amount"])}</div>
            </div>);
        }
        else {
            return "";
        }
    }

    showToolTip = (e) => {
        this.setState({
            activeTooltip: true
        })
    }

    hideToolTip = (e) => {
        this.setState({
            activeTooltip: false
        })
    }

    onBarClick = category => {
        this.props.updateCategoryFilterTopCats(category);
    }

    CategoryTick = props => {
       return <foreignObject x={props.x - 10} y={props.y} width={20} height={30}><i style={{color: '#b7b7b7'}} className={this.props.data[props.payload.value].icon}></i></foreignObject>
    }

    render() {
        const { Title } = Typography;
        return (
            <div>
                <Title className="chart-title" level={4}>Top categories </Title>
                <ResponsiveContainer
                    width="100%"
                    height={200}
                >
                    <BarChart
                     margin={{ top: 30, right: 20, left: 20, bottom: 0 }}
                        barSize={10}
                        data={this.props.data}
                    >
                        <Tooltip active={this.state.activeTooltip} cursor={false} content={this.customTooltip} />
                        <CartesianGrid stroke="#b7b7b7" vertical={false} />
                        <XAxis  axisLine={false} tickLine={false} tick={<this.CategoryTick/>} />
                        <YAxis orientation="right" tickMargin={20}  tick={{ fill: '#b7b7b7' }} unit="€" axisLine={false} tickLine={false} />
                        <Bar onClick={this.onBarClick} isAnimationActive={true} onMouseLeave={this.hideToolTip} onMouseOver={this.showToolTip} dataKey="amount" fill="#b7b7b7" radius={[5, 5, 0, 0]} >
                            {
                                this.props.data.map((category, index) => {
                                    return <Cell key={index} fill={this.props.filteredCategories.indexOf(category.id) > -1 ? category.color : "rgba(183, 183, 183,0.5)"} />;
                                })
                            }
                        </Bar>
                    </BarChart>
                </ResponsiveContainer>
                </div>
        )
    }
}