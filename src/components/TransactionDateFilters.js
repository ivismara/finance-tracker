import React from 'react';

import { Modal , Button, Popover } from 'antd';
import  Div100vh from 'react-div-100vh'

import moment from 'moment'

import { CustomRangePickerFilter } from './CustomRangePickerFilter'

export class TransactionDateFilters extends React.Component {
    constructor(props) {
        super(props)


        this.state = {
            isDateFilterMobileModalVisible: false,
            isDateFilterPopoverVisible: false,
            activeFilter: 'alltime'
        }

        this.showMobileModalDateFilter = this.showMobileModalDateFilter.bind(this)
    }

    handleRemoveDateFilter = () => {
        this.props.handleRemoveDateFilter();
        this.hideMobileModalDateFilter()
    }


    showMobileModalDateFilter = () => {
        this.setState({
            isDateFilterMobileModalVisible: true
        })
    }

    hideMobileModalDateFilter = () => {
        this.setState({
            isDateFilterMobileModalVisible: false
        });
    };

    handleUpdateDateFilter = (startDate, endDate, activeFilter) => {
        if(typeof activeFilter === 'undefined'){
            activeFilter = 'custom'
        }

        this.setState({
            displayingElements: 5,
            activeFilter: activeFilter
        })

        if(startDate === null ){
            startDate = new moment(this.props.userCreationDate);
        }

        if(endDate === null) {
            endDate = new moment().add(10, 'days');
        }

        this.props.handleUpdateDateFilter(startDate, endDate, activeFilter);
    }

    handlePopoverVisibleChange = isDateFilterPopoverVisible => {
        this.setState({ isDateFilterPopoverVisible });
    };

    componentDidUpdate = prevProps => {
        if(this.props.filteredDates !== prevProps.filteredDates && moment(this.props.filteredDates.startDate).isSame(moment(this.props.userCreationDate)) && moment(this.props.filteredDates.endDate).isSame(new moment().add(10, 'days'), 'day')){
            this.setState({
                activeFilter: 'alltime'
            })
        }
    }

    render() {
        return (
            <div className="date-filter-container">  
                <Button size={window.matchMedia("(max-width: 1199.98px)").matches ? 'small' : 'default'} className={this.state.activeFilter === 'alltime' ? 'filter-btn filter-active mobile-filter-btn' : 'filter-btn mobile-filter-btn'} onClick={ () => this.handleUpdateDateFilter(moment(this.props.userCreationDate), moment().add(10, 'days'), 'alltime')}  type={this.state.activeFilter === 'alltime' ? 'primary' : 'link'}>All time</Button>
                <Button size={window.matchMedia("(max-width: 1199.98px)").matches ? 'small' : 'default'} className={this.state.activeFilter === 'week' ? 'filter-btn filter-active mobile-filter-btn' : 'filter-btn mobile-filter-btn'} onClick={ () => this.handleUpdateDateFilter(moment().startOf('week'), moment().endOf('week'), 'week')}  type={this.state.activeFilter === 'week' ? 'primary' : 'link'}>Week</Button>
                <Button size={window.matchMedia("(max-width: 1199.98px)").matches ? 'small' : 'default'} className={this.state.activeFilter === 'month' ? 'filter-btn filter-active mobile-filter-btn' : 'filter-btn mobile-filter-btn'} onClick={ () => this.handleUpdateDateFilter(moment().startOf('month'), moment().endOf('month'), 'month')} type={(this.state.activeFilter === 'month' || (this.props.filteredDates.startDate === null &&  this.props.filteredDates.endDate === null)) ? 'primary' : 'link'}>Month</Button>
                <Button size={window.matchMedia("(max-width: 1199.98px)").matches ? 'small' : 'default'} className={this.state.activeFilter === 'year' ? 'filter-btn filter-active mobile-filter-btn' : 'filter-btn mobile-filter-btn'}  onClick={ () => this.handleUpdateDateFilter(moment().startOf('year'), moment().endOf('year'), 'year')}  type={this.state.activeFilter === 'year' ? 'primary' : 'link'}>Year</Button>
            {window.matchMedia("(max-width: 575.98px)").matches ? (
                <span>
                    <Modal
                        className="filter-mobile-modal"
                        visible={this.state.isDateFilterMobileModalVisible}
                        onCancel={this.hideMobileModalDateFilter}
                        onOk={this.hideMobileModalDateFilter}
                        footer={null}
                    >
                        <Div100vh className="custom-filter-mobile-container">
                            <CustomRangePickerFilter filteredDates={this.props.filteredDates} updateDateFilter={(startDate, endDate) => this.handleUpdateDateFilter(startDate, endDate)} />
                            <div className="filter-custom-footer">
                                <Button style={{ color: '#3B3B3B' }} key="back" type="link" onClick={this.handleRemoveDateFilter}>
                                    Cancel
                            </Button>
                                <Button key="submit" type="primary" onClick={this.hideMobileModalDateFilter}>
                                    Apply Filter
                            </Button>
                            </div>
                        </Div100vh>
                    </Modal>
                    <Button size={window.matchMedia("(max-width: 1199.98px)").matches ? 'small' : 'default'} className={this.state.activeFilter === 'custom' ? 'filter-btn filter-active mobile-filter-btn' : 'filter-btn mobile-filter-btn custom-dates'} onClick={this.showMobileModalDateFilter}type={this.state.activeFilter !== 'custom' ? 'link ' : 'primary'}>Custom</Button>
                </span>
            ) : (
                    <Popover visible={this.state.isDateFilterPopoverVisible} onVisibleChange={this.handlePopoverVisibleChange} overlayClassName="date-filter" getPopupContainer={trigger => trigger.parentNode} trigger="click" placement="bottomRight" content={<CustomRangePickerFilter filteredDates={this.props.filteredDates} updateDateFilter={(startDate, endDate) => this.handleUpdateDateFilter(startDate, endDate)} />}>
                        <Button size={window.matchMedia("(max-width: 1199.98px)").matches ? 'small' : 'default'} className={this.state.activeFilter === 'custom' ? 'filter-btn filter-active mobile-filter-btn' : 'filter-btn mobile-filter-btn custom-dates'} type={this.state.activeFilter !== 'custom' ? 'link ' : 'primary'}>Custom</Button>
                    </Popover>
                )
            } </div>
        )
    }
}