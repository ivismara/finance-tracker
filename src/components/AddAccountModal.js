import React from 'react';
import { Modal, Button, Icon } from 'antd';
import { WrappedNewAccountForm } from './NewAccountForm';


export class AddAccountModal extends React.Component {

  constructor(props){
    super(props);
    this.state = { visible: false }
    this.handleAccountAdded = this.handleAccountAdded.bind(this);
  }
  

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false,
    });
  };

  handleAccountAdded = (e,newAccount) => {
    this.setState({
      visible: false,
    });
    this.props.handleAccountAdded(e,newAccount);
  };

  render() {
    return (
      <div>
        <Button style={{ width: '100%' }} type="dashed" onClick={this.showModal}>
          <Icon type="plus" />
          <span>Add Account</span>
        </Button>
        <Modal
          footer={null}
          title={this.props.firstAccountNeeded === false ? "Add a new Account" : "Add your first account in order to get started"}
          visible={this.props.firstAccountNeeded === false ? this.state.visible : true}
          onCancel={this.props.firstAccountNeeded === false ? this.handleCancel : ()=>{}}
          destroyOnClose="true"
          closable={this.props.firstAccountNeeded === false ? true : false}

        >
          <WrappedNewAccountForm handleAccountAdded={(e,newAccount) => this.handleAccountAdded(e,newAccount)} />
        </Modal>
      </div>
    );
  }
}