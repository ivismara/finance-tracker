import React from 'react';
import {DayPickerSingleDateController} from 'react-dates';




export class CustomSingleDatePicker extends React.Component{
    constructor(props){
        super(props)

        this.state = ({
            focused: true,
        })
    }

    handleDateChange = date => {
        this.props.handleDateChange(date);
    }

    render(){
        return( 
            <DayPickerSingleDateController
                daySize={30}
                hideKeyboardShortcutsPanel={true}
                enableOutsideDays={true}
                navPrev={<span style={{ color: '#3B3B3B', position: 'absolute', fontSize: 20, top: 18, left: 30 }} className="uil uil-previous"></span>}
                navNext={<span style={{ color: '#3B3B3B', position: 'absolute', fontSize: 20, top: 18, right: 30 }} className="uil uil-step-forward"></span>}
                onDateChange={this.handleDateChange}
                date={this.props.transactionDate}
                focused={this.state.focused}
                onFocusChange={({ focused }) => this.setState({ focused })}
            />
        )
    }
}