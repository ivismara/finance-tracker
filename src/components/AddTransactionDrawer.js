import React from 'react';
import { Drawer, Button, Icon, } from 'antd';
import  NewTransactionForm  from './NewTransactionForm';

import './AddTransactionDrawer.css'

export class AddTransactionDrawer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { visible: false };
    this.handleTransactionAdded = this.handleTransactionAdded.bind(this);
  }

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  handleTransactionAdded = (e,newTransaction) => {
    this.setState({
      visible: false,
    });
    this.props.handleTransactionAdded(e,newTransaction);
  };

  render() {
    return (
      <div className="add-transaction-button-container">
          <Button className="add-transaction-button" type="primary" onClick={this.showDrawer}>
            <i style={{fontSize: 20}} className="uil uil-plus-circle"></i> Add Transaction
          </Button>
        <Drawer
          title="Add a transaction"
          width={400}
          onClose={this.onClose}
          visible={this.state.visible}
          bodyStyle={{ paddingBottom: 24 }}
        >
          <NewTransactionForm selectedAccount={this.props.selectedAccount} formatToEur={(e) => this.props.formatToEur(e)} handleTransactionAdded={(e,newTransaction) => this.handleTransactionAdded(e,newTransaction)} accountList={this.props.accountList} categories={this.props.categories}/>
        </Drawer>
      </div>
    );
  }
}


