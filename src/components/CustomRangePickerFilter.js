import React from 'react';

import { DayPickerRangeController } from 'react-dates';

import 'react-dates/lib/css/_datepicker.css';
import '../style/react_dates_overrides.css';



export class CustomRangePickerFilter extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            startDate: this.props.filteredDates.startDate,
            endDate: this.props.filteredDates.endDate,
            focusedInput: 'startDate'
        }
    }

    handleDateChange = ({ startDate, endDate }) => {
        this.props.updateDateFilter(startDate, endDate);
        this.setState({ startDate, endDate })
    }

    handleFocusChange = focusedInput => {
        this.setState({ focusedInput: !focusedInput ? 'startDate' : focusedInput, })
    }


    componentDidUpdate = prevProps => {
        if(this.props.filteredDates.startDate !== prevProps.filteredDates.startDate || this.props.filteredDates.endDate !== prevProps.filteredDates.endDate){
            this.setState({
                startDate: this.props.filteredDates.startDate,
                endDate: this.props.filteredDates.endDate
            })
        }
    }

    render() {

        return (
                <DayPickerRangeController
                    daySize={window.matchMedia("(max-width: 575.98px)").matches ? 50 : 30}
                    orientation={window.matchMedia("(max-width: 575.98px)").matches ? 'verticalScrollable' : 'horizontal'}
                    numberOfMonths={window.matchMedia("(max-width: 575.98px)").matches ? 6 : 1}
                    withFullScreenPortal
                    noBorder
                    hideKeyboardShortcutsPanel={true}
                    enableOutsideDays={window.matchMedia("(max-width: 575.98px)").matches ? false: true}
                    navPosition="navPositionTop"
                    navPrev={window.matchMedia("(max-width: 575.98px)").matches ? <div className="mobile-top-arrow" style={{textAlign: 'center'}}><span style={{ color: '#7D7A77', fontSize: 20 }} className="uil uil-arrow-up"></span></div> : <span style={{ color: '#7D7A77', position: 'absolute', fontSize: 20, top: 18, left: 30 }} className="uil uil-previous"></span>}
                    navNext={window.matchMedia("(max-width: 575.98px)").matches ? <div style={{textAlign: 'center'}}><span style={{ color: '#7D7A77', fontSize: 20 }} className="uil uil-arrow-down"></span></div> :<span style={{ color: '#7D7A77', position: 'absolute', fontSize: 20, top: 18, right: 30 }} className="uil uil-step-forward"></span>}
                    startDate={this.state.startDate}
                    endDate={this.state.endDate}
                    onDatesChange={this.handleDateChange}
                    focusedInput={this.state.focusedInput}
                    onFocusChange={this.handleFocusChange}
                />
        )
    }
}