import React from 'react';

import { Form, Select } from 'antd';

class MoneyTransferForm extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        const { getFieldDecorator } = this.props.form;
        const { Option } = Select;
        return (
            <Form onSubmit={this.handleSubmit}>
                  <Form.Item label="Account">
                    {getFieldDecorator('giverAccount', {
                        rules: [{ required: true, message: 'Please select an account' }],
                        initialValue: this.props.selectedAccount === 'overview' ? this.props.accountList[0].id : this.props.selectedAccount
                    })(
                        <Select>
                            {/*da sostituire con eventuale id*/}
                            {this.props.accountList.map((account, index) => (
                                <Option value={account.id} key={index}>{account.name}</Option>
                            ))}
                        </Select>
                    )}
                </Form.Item>
                <Form.Item label="Account">
                    {getFieldDecorator('recieverAccount', {
                        rules: [{ required: true, message: 'Please select an account' }],
                        initialValue: this.props.selectedAccount === 'overview' ? this.props.accountList[0].id : this.props.selectedAccount
                    })(
                        <Select>
                            {/*da sostituire con eventuale id*/}
                            {this.props.accountList.map((account, index) => (
                                <Option value={account.id} key={index}>{account.name}</Option>
                            ))}
                        </Select>
                    )}
                </Form.Item>
            </Form>
        )
    }
}

export const WrappedMoneyTransferForm = Form.create({ name: 'new_money_transfer' })(MoneyTransferForm);