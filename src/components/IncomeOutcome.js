import React from 'react';
import { Typography, Card } from 'antd';

import './IncomeOutcome.css'

export class IncomeOutcome extends React.Component {

    render() {
        const { Text, Title } = Typography;

        return (
            <div className="income-outcome-container">
                <Card className="income-outcome-card standard-card" bordered={false}>
                
                        <Text className="income-outcome-title-text">Income</Text>
                        <Title className="income-outcome-value" style={{ paddingTop: 5 }} level={4}>{this.props.globalIncome}</Title>
                </Card>
                <Card className="income-outcome-card standard-card" bordered={false}>
                        <Text className="income-outcome-title-text" >Outcome</Text>
                        <Title className="income-outcome-value" style={{ paddingTop: 5 }} level={4}>{this.props.globalOutcome}</Title></Card>
            </div>
        )
    }
}