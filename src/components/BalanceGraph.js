import React from 'react';
import moment from 'moment';

import { Typography } from 'antd'

import { AreaChart, XAxis, YAxis, CartesianGrid, Tooltip, Area, ResponsiveContainer } from 'recharts';

export class BalanceGraph extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            chartData: [],
            activeTooltip: false,
        }
    }

    componentDidUpdate = prevProps => {
        if (this.props.filteredTransactions !== prevProps.filteredTransactions) {
            this.getChartData();
        }

    }

    enumerateDaysBetweenDates = (startDate, endDate) => {
        var dates = [];

        var currDate = moment(startDate).startOf('day').subtract(1, 'day');
        var lastDate = moment(endDate).startOf('day').add(1, 'days');

        while (currDate.add(1, 'days').diff(lastDate) < 0) {
            dates.push(currDate.clone().toDate());
        }

        return dates;
    }

    getChartData = () => {

        let startDate = this.props.filteredDates.startDate;
        let endDate = this.props.filteredDates.endDate;


        const dates = this.enumerateDaysBetweenDates(startDate, endDate);
        let chartData = [];

        dates.forEach(date => {
            let income = 0;
            let outcome = 0;
            this.props.filteredTransactions.forEach(transaction => {
                if (moment(transaction.date).isSame(moment(date), 'day')) {
                    if (transaction.amount > 0) {
                        income += transaction.amount;
                    } else {
                        outcome += Math.abs(transaction.amount)
                    }
                }
            })

            const chartDate = new Object();
            chartDate.date = moment(date).format('MMM DD YYYY');
            chartDate.income = income;
            chartDate.outcome = outcome;

            chartData.push(chartDate)
        })

        this.setChartData(chartData);
    }

    setChartData = chartData => {
        this.setState({
            chartData: chartData,
        })
    }

    customTooltip = e => {
        if (e.payload != null && e.payload[0] != null) {
            return (<div className="custom-tooltip">
                <div style={{ color: e.payload[0].payload["color"], fontWeight: 'bold' }} >{e.payload[0].payload["date"]}</div>
                <div style={{ color: '#7D7A77' }}>{this.props.formatToEur(e.payload[0].payload["income"])}</div>
                <div style={{ color: '#7D7A77' }}>{this.props.formatToEur(e.payload[0].payload["outcome"])}</div>
            </div>);
        }
        else {
            return "";
        }
    }


    showToolTip = (e) => {
        this.setState({
            activeTooltip: true
        })
    }

    hideToolTip = (e) => {
        this.setState({
            activeTooltip: false
        })
    }

    CustomDateTick = props => {
        if(this.props.activeDateFilter === 'week'){
            return <text x={props.x - 10} style={{fontSize: 10, fill: '#b7b7b7' }} y={props.y + 20} width={40} height={20}>{moment(props.payload.value).format('ddd')}</text>
        }else if(this.props.activeDateFilter === 'year'){
            return <text x={props.x + (window.matchMedia("(max-width: 575.98px)").matches ? 10 : 25)} textAnchor="middle" style={{fontSize: 10, fill: '#b7b7b7' }} y={props.y + 20} width={40} height={20}>{moment(props.payload.value).format('MMM')}</text>
        }else{
            return null
        }
    }

    render() {

        const { Title} = Typography

        return (<div>

<Title className="chart-title" level={4}>Income and expenses</Title>
            <ResponsiveContainer
                width="100%"
                height={200}
            >
                <AreaChart data={this.state.chartData}
                    margin={{ top: 30, right: 20, left: 20, bottom: 0 }}>
                    <defs>
                        <linearGradient id="income" x1="0" y1="0" x2="0" y2="1">
                            <stop offset="0%" stopColor="#0be881" stopOpacity={0.2} />
                            <stop offset="90%" stopColor="#0be881" stopOpacity={0} />
                        </linearGradient>
                        <linearGradient id="outcome" x1="0" y1="0" x2="0" y2="1">
                            <stop offset="0%" stopColor="#ff5e57" stopOpacity={0.2} />
                            <stop offset="90%" stopColor="#ff5e57" stopOpacity={0} />
                        </linearGradient>
                    </defs>
                    <XAxis
                        tick={<this.CustomDateTick/>}
                        axisLine={false}
                        interval={this.props.activeDateFilter === 'year' ? 30 : 0}
                        tickLine={false}
                        dataKey="date" />
                    <YAxis tickMargin={20} axisLine={false} unit="€" tick={{ fill: '#b7b7b7' }} tickLine={false} orientation="right" />
                    <CartesianGrid stroke="#b7b7b7" vertical={false} />
                    <Tooltip cursor={false} content={this.customTooltip} />
                    <Area animationDuration={500} type="monotone" dataKey="income" strokeWidth={2} stroke="#0be881" fillOpacity={1} fill="url(#income)" />
                    <Area animationDuration={500} type="monotone" dataKey="outcome" strokeWidth={2} stroke="#ff5e57" fillOpacity={1} fill="url(#outcome)" />
                </AreaChart>
            </ResponsiveContainer>
        </div>
        )
    }
}