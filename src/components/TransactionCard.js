import React from 'react';
import { Avatar, Modal, Typography, Popover, Button, Select, InputNumber, Tag } from 'antd';
import { RIEInput } from 'riek';
import moment from 'moment';
import { CustomSingleDatePicker } from './CustomSingleDatePicker';
import CurrencyInput from 'react-currency-input';
import Div100vh from 'react-div-100vh'
import { motion } from "framer-motion"

import 'react-dates/lib/css/_datepicker.css';
import '../style/react_dates_overrides.css';
import './TransactionCard.css';

export class TransactionCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            transactionEdited: false
        }

        this.showTransactionModal = this.showTransactionModal.bind(this);
        this.updateTransaction = this.updateTransaction.bind(this);
        this.deleteTransaction = this.deleteTransaction.bind(this)
    }

    showTransactionModal = () => {
        this.setState({
            visible: true,
            transactionDesc: this.props.transaction.description,
            transactionDate: new moment(this.props.transaction.date),
            transactionCategory: this.props.transaction.category,
            transactionAmount: this.props.transaction.amount,
            transactionAccount: this.props.transaction.account
        })
    }

    handleCancel = e => {
        this.setState({
            visible: false,
            transactionEdited: false
        })
    };


    isDescriptionOK = desc => {
        return (desc.length <= 32 && desc.length > 0)
    }


    SaveChangesButton = () => {
        if (this.state.transactionEdited) {
            return <Button onClick={this.updateTransaction} className="save-transaction-edit" type="primary" style={{ marginTop: 'auto' }}>Save Changes</Button>
        } else {
            return null
        }
    }

    textInputChange = newText => {
        this.setState({
            transactionDesc: newText.transactionDesc,
            transactionEdited: true
        })
    }

    handleDateChange = date => {
        this.setState({
            transactionDate: date,
            transactionEdited: true
        })
    }

    onAccountChange = (account) => {
        if (account !== this.state.transactionAccount) {
            this.setState({
                transactionAccount: account,
                transactionEdited: true
            })
        }
    }

    onCategoryChange = (cagtegory) => {
        if (cagtegory !== this.state.transactionCategory) {
            this.setState({
                transactionCategory: cagtegory,
                transactionEdited: true
            })
        }
    }

    handleAmountChange = (event, maskedvalue, floatvalue) => {
        if (floatvalue === 0) {
            document.querySelector('.custom-input-error').style['display'] = 'block';
            this.setState({
                transactionAmount: floatvalue,
                transactionEdited: false
            });
        } else if (floatvalue !== this.state.transactionAmount) {
            document.querySelector('.custom-input-error').style['display'] = 'none';
            this.setState({
                transactionAmount: floatvalue,
                transactionEdited: true
            });
        }
    }

    updateTransaction = () => {
        if (this.state.transactionAmount === 0) {
            document.querySelector('.custom-input-error').style['display'] = 'block'
        } else {
            const updatedTransaction = new Object();
            updatedTransaction.account = this.state.transactionAccount;
            updatedTransaction.category = this.state.transactionCategory;
            updatedTransaction.amount = this.state.transactionAmount;
            updatedTransaction.date = this.state.transactionDate;
            updatedTransaction.description = this.state.transactionDesc;

            this.setState({
                visible: false
            })
            this.props.handleUpdateTransaction(this.props.transaction.id, updatedTransaction)
        }

    }

    deleteTransaction = () => {
        this.props.handleDeleteTransaction(this.props.transaction.id, this.props.transaction.account, this.props.transaction.amount)
    }

    editTransactionModalContent = () => {
        const { Title, Text } = Typography;
        const { Option } = Select;
        return (
            <div class="transaction-modal-content">
                <Title style={{ paddingTop: 20 }} level={2} >
                    <RIEInput
                        className="transaction-modal-desc"
                        value={this.state.transactionDesc}
                        propName="transactionDesc"
                        change={this.textInputChange}
                        validate={this.isDescriptionOK}
                    />
                </Title>
                <div className="transaction-modal-properties-container">
                    <div className="transaction-modal-property">
                        <div className="transaction-modal-property-name"><span className="uil uil-calender"></span><Text style={{ color: '#3B3B3B', paddingLeft: 5 }}>Type</Text></div>
                        <div className="transaction-modal-property-value"><Tag color={this.state.transactionAmount < 0 ? '#ff5e57' : '#0be881'}>{this.state.transactionAmount < 0 ? 'Outcome' : 'Income'}</Tag></div>
                    </div>
                    <div className="transaction-modal-property">
                        <div className="transaction-modal-property-name"><span className="uil uil-calender"></span><Text style={{ color: '#3B3B3B', paddingLeft: 5 }}>Date</Text></div>
                        <Popover
                            overlayClassName="date-edit"
                            content={<CustomSingleDatePicker handleDateChange={date => this.handleDateChange(date)} />}
                            trigger="click"
                            placement="bottomLeft"
                        >
                            <Button className="transaction-modal-property-value" type="link">{moment(this.state.transactionDate).format('MMMM DD YYYY')}</Button>
                        </Popover>
                    </div>
                    <div className="transaction-modal-property">
                        <div className="transaction-modal-property-name"><span className="uil uil-tag"></span><Text style={{ color: '#3B3B3B', paddingLeft: 5 }}>Category</Text></div>
                        <div className="transaction-modal-property-value">
                            <Select
                                onChange={this.onCategoryChange}
                                defaultValue={this.state.transactionCategory}
                                className="antd-input-reset"
                                showSearch
                                suffixIcon={<span></span>}
                                style={{ width: '100%' }}
                                optionFilterProp="children"
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {this.props.categories.map((category) => (
                                    <Option style={{ borderLeft: '3px solid', borderLeftColor: category.color }} key={category.id}>{category.name}</Option>
                                ))}
                            </Select>
                        </div>
                    </div>
                    <div className="transaction-modal-property">
                        <div className="transaction-modal-property-name"><span className="uil uil-bag"></span><Text style={{ color: '#3B3B3B', paddingLeft: 5 }}>Account</Text></div>
                        <div className="transaction-modal-property-value">
                            <Select onChange={this.onAccountChange} defaultValue={this.state.transactionAccount} suffixIcon={<span></span>} style={{ width: '100%' }} className="antd-input-reset">
                                {this.props.accountList.map((account, index) => (
                                    <Option value={account.id} key={index}>{account.name}</Option>
                                ))}
                            </Select></div>
                    </div>
                    <div className="transaction-modal-property">
                        <div className="transaction-modal-property-name"><span className="uil uil-coins"></span><Text style={{ color: '#3B3B3B', paddingLeft: 5 }}>Amount</Text></div>
                        <div className="transaction-modal-property-value">

                            <div className="ant-form-item-control has-error" style={{ position: 'relative' }}>
                                <CurrencyInput allowNegative={true} thousandSeparator=" " decimalSeparator="," suffix="€" className="ant-input-number antd-input-reset" style={{ width: '100%' }} value={this.state.transactionAmount} onChangeEvent={this.handleAmountChange} />
                                <div class="ant-form-explain custom-input-error" style={{ display: 'none', position: 'absolute' }}>Amount can't be 0!</div>
                            </div>
                        </div>
                    </div>
                </div>
                <this.SaveChangesButton />
            </div>
        )
    }


    render() {
        

        const interactionIcon = {
            hidden: { opacity: 0, x: -10 },
            visible: i => ({
                opacity: 1,
                x: 0,
                transition: {
                    delay: i * 0.15
                }
            })
        }

        return (
            <div>
                {window.matchMedia("(max-width: 575.98px)").matches ? (
                    <Modal
                        className="transaction-modal"
                        footer={null}
                        title={null}
                        visible={this.state.visible}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                    >
                        <Div100vh>
                            <this.editTransactionModalContent />
                        </Div100vh>
                    </Modal>
                ) : (
                        <Modal
                            className="transaction-modal"
                            footer={null}
                            title={null}
                            visible={this.state.visible}
                            onOk={this.handleOk}
                            onCancel={this.handleCancel}
                        ><this.editTransactionModalContent />
                        </Modal>
                    )}
                <motion.div initial={window.matchMedia("(max-width: 1199.98px)").matches ? "visible" : "hidden"} whileHover="visible" className="transaction-card">
                    <Avatar size="large" shape="square" style={{ backgroundColor: this.props.categories.find(({ id }) => id === this.props.transaction.category).color }}>
                        <i style={{ height: '100%', width: '100%', fontSize: 26 }} className={this.props.categories.find(({ id }) => id === this.props.transaction.category).icon}></i>
                    </Avatar>
                    <div className="transaction-info-left">
                        <div className='transaction-description'> {this.props.transaction.description} </div>
                        <div className='transaction-account'> {this.props.accountList.find(({ id }) => id === this.props.transaction.account).name}</div>
                    </div>
                    <div className="transaction-info-right">
                        <div className={this.props.transaction.amount > 0 ? 'income transaction-amount' : 'outcome transaction-amount'}> {this.props.formatToEur(this.props.transaction.amount)}</div>
                        <div className='transaction-date'>{this.props.transaction.date}</div>
                    </div>
                    <motion.div variants={interactionIcon} className="transaction-interaction-icon transaction-edit-icon"> <Button onClick={this.showTransactionModal} shape="circle" type="link"><span className="uil uil-pen"></span> </Button></motion.div>
                    <motion.div custom={1} variants={interactionIcon} className="transaction-interaction-icon transaction-delete-icon"> <Button onClick={this.deleteTransaction} shape="circle" type="link"><span className="uil uil-trash-alt"></span> </Button></motion.div>
                </motion.div>
            </div>
        )
    };
}