import React from 'react';
import {Modal} from 'antd';
import { WrappedMoneyTransferForm } from './MoneyTransferForm'

export class MoneyTransferModal extends React.Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <Modal
            visible={this.props.visible}
            footer={null}
            onCancel={this.props.closeMoneyTransferModal}
            >
                <WrappedMoneyTransferForm accountList={this.props.accountList}  selectedAccount={this.props.selectedAccount}/>
            </Modal>
        )
    }
}