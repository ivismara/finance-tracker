import React from 'react';
import { Form, Icon, Input, Button, InputNumber, Select } from 'antd';

import CurrencyInput from 'react-currency-input';

class NewAccountForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            amount: 0.00
        }
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.handleAccountAdded(e, values);
            }
        });
    };

    handleAmountChange = (event, maskedvalue, floatvalue) => {
        this.props.form.setFieldsValue({
            balance: floatvalue,
        });

        this.setState({
            amount: floatvalue
        })

    }



    render() {
        const { getFieldDecorator } = this.props.form;
        const { Option } = Select;
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Item label="Account Name">
                    {getFieldDecorator('name', {
                        rules: [{ required: true, message: 'Please input the account name!' }],
                    })(
                        <Input
                            prefix={<Icon type="edit" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="eg. Main Bank"
                        />,
                    )}
                </Form.Item>
                <Form.Item label="Account Balance">
                    {getFieldDecorator('balance', {
                        initialValue: this.state.amount,
                        rules: [{ required: true, message: 'Balance is required!' }]
                    })(
                        <div>
                            <Input precision={2} style={{ width: '100%', display: 'none' }} />
                            <CurrencyInput autoFocus={false} thousandSeparator=" " decimalSeparator="," suffix="€" className="ant-input-number" style={{ width: '100%', padding: '4px 11px' }} value={this.state.amount} onChangeEvent={this.handleAmountChange} />
                        </div>
                    )}
                </Form.Item>
                <Form.Item style={{ marginBottom: 0 }} label="Salary">
                    <Form.Item style={{ display: 'inline-block', width: 'calc(40% - 32px)' }}>
                        {getFieldDecorator('salary')(
                            <InputNumber style={{ width: '100%' }}
                                formatter={value => `€ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                parser={value => value.replace(/\€\s?|(,*)/g, '')}
                            />,
                        )}
                    </Form.Item>
                    <span style={{ display: 'inline-block', width: 'auto', paddingLeft: 12, paddingRight: 12 }}>every</span>
                    <Form.Item style={{ display: 'inline-block', width: '30%' }}>
                        {getFieldDecorator('payday')(
                            <Select>
                                <Option value="1">1st</Option>
                                <Option value="2">2nd</Option>
                                <Option value="3">3rd</Option>
                                <Option value="4">4th</Option>
                                <Option value="5">5th</Option>
                                <Option value="6">6th</Option>
                                <Option value="7">7th</Option>
                                <Option value="8">8th</Option>
                                <Option value="9">9th</Option>
                                <Option value="10">10th</Option>
                                <Option value="11">11th</Option>
                                <Option value="12">12th</Option>
                                <Option value="13">13th</Option>
                                <Option value="14">14th</Option>
                                <Option value="15">15th</Option>
                                <Option value="16">16th</Option>
                                <Option value="17">17th</Option>
                                <Option value="18">18th</Option>
                                <Option value="19">19th</Option>
                                <Option value="20">20th</Option>
                                <Option value="21">21st</Option>
                                <Option value="22">22th</Option>
                                <Option value="23">23th</Option>
                                <Option value="24">24th</Option>
                                <Option value="25">25th</Option>
                                <Option value="26">26th</Option>
                                <Option value="27">27th</Option>
                                <Option value="28">28th</Option>
                                <Option value="29">29th</Option>
                                <Option value="30">30th</Option>
                                <Option value="31">31st</Option>
                            </Select>
                        )}
                    </Form.Item>
                    <span style={{ display: 'inline-block', width: 'auto', paddingLeft: 12, paddingRight: 12 }}>of the month.</span>
                </Form.Item>
                <Form.Item style={{ marginBottom: 0 }}>

                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Add Account!
            </Button>
                </Form.Item>
            </Form>
        );
    }
}

export const WrappedNewAccountForm = Form.create({ name: 'new_account' })(NewAccountForm);