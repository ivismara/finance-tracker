import React from 'react';
import {Typography} from 'antd'

export class CurrencyStatText extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        const {Title} = Typography;
        return <Title level={this.props.titleLevel}>{this.props.statValue}</Title>
    }
}

export default CurrencyStatText;