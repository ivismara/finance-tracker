import React from "react";
import { Layout } from 'antd';


export class Settings extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { Content } = Layout;
    return (
      <Content>
        <div>Settings</div>
      </Content>
    );
  }
}

export default Settings;