import React from "react";
import { Layout } from 'antd';


export class Savings extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { Content } = Layout;
    return (
      <Content>
        <div>Savings</div>
      </Content>
    );
  }
}

export default Savings;