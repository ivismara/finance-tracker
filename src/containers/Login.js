import React from 'react';
import { Layout, Form, Input, Icon, Button, Card, Typography, Tabs, notification } from 'antd';
import { Redirect } from 'react-router-dom';

import moment from 'moment'
import './Login.css';
import firebase from 'firebase';


export class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toHome: false,
            standardCategories : {}
        }

    }


    handleSignInSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields(['loginemail', 'loginpassword'], (err, values) => {
            if (!err) {
                firebase.auth().signInWithEmailAndPassword(values.loginemail, values.loginpassword).catch(function (error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    let errorDescription = "";

                    switch (errorCode) {
                        case "auth/user-not-found":
                            errorDescription = "Oh! It seems that your email is new to us. Register by clicking the \"Sign Up\" tab. ";
                            break;
                        case "auth/wrong-password":
                            errorDescription = "Invalid password, please retry!"
                            break;
                        default:
                            errorDescription = error.message;
                            break;
                    }

                    notification.open({
                        message: 'Error!',
                        description: errorDescription,
                        icon: <Icon type="frown" style={{ color: '#ff3f34' }} />,
                    });


                    // ...
                }).then(
                    firebase.auth().onAuthStateChanged(user => {
                        if (user) {
                            console.log('logged')
                            this.setState(() => ({
                                toHome: true,
                            }))
                        }
                    })
                );
            }
        });
    };

    componentDidMount() {
        return firebase.database().ref('/categories/').once('value').then( snapshot => {
            let standardCategories = snapshot.val();

            this.setState({
                standardCategories : standardCategories
            })
        })
    }


    handleSignUpSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields(['email', 'password', 'firstname'], (err, values) => {
            if (!err) {
                firebase.auth().createUserWithEmailAndPassword(values.email, values.password).catch(function (error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    let errorDescription = "";

                    switch (errorCode) {
                        case "auth/email-already-in-use":
                            errorDescription = "Provided email is already in use. Please choose another one or try to login";
                            break;
                        case "auth/invalid-email":
                            errorDescription = "Provided email is invalid. Please enter another one!";
                            break;
                        case "auth/weak-password":
                            errorDescription = "Please choose a stronger password! Use at least 6 characters.";
                            break;
                        default:
                            errorDescription = error.message;
                            break;
                    }

                    notification.open({
                        message: 'Error!',
                        description: errorDescription,
                        icon: <Icon type="frown" style={{ color: '#ff3f34' }} />,
                    });
                }).then((user) => {
                    if (user) {
                        firebase.database().ref('users/' + firebase.auth().currentUser.uid).set({
                            firstName: values.firstname,
                            signUpDate: new moment().format('MMM DD YYYY').toString(),
                            accountList: [], //check in wrapper se è inesistente creazione
                            savings: [],    //trovare un modo per inizializzare a 0 
                            transactions: [], //trovare un modo per inizializzare a 0
                            categories: this.state.standardCategories //pusharle da un vettore predefinito
                        }).then(
                            this.setState({
                                toHome: true
                            })
                        )

                    } else {

                    }
                }
                );
            }
        });
    };

    render() {
        const { Content } = Layout;
        const { Title } = Typography;
        const { TabPane } = Tabs;
        const { getFieldDecorator } = this.props.form;

        if (this.state.toHome === true) {
            return <Redirect to='/' />
        }

        return (
            <Layout>
                <Content>
                    <Card className="sign-form-card">
                        <Tabs tabBarGutter={0} defaultActiveKey="1">
                            <TabPane tab="Sign in" key="1" >
                                <Title level={3}>Login</Title>
                                <Form onSubmit={this.handleSignInSubmit} className="sign-form">
                                    <Form.Item>
                                        {getFieldDecorator('loginemail', {
                                            rules: [
                                                {
                                                    required: true,
                                                    message: 'Please input your email!'
                                                },
                                                {
                                                    type: 'email',
                                                    message: 'The input is not valid E-mail!',
                                                },
                                            ],
                                        })(
                                            <Input
                                                prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                                placeholder="Email"
                                            />,
                                        )}
                                    </Form.Item>
                                    <Form.Item >
                                        {getFieldDecorator('loginpassword', {
                                            rules: [{ required: true, message: 'Please input your Password!' }],
                                        })(
                                            <Input.Password
                                                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                                type="password"
                                                placeholder="Password"
                                            />,
                                        )}
                                    </Form.Item>
                                    <div style={{ display: 'flex', justifyContent: 'flex-end', paddingBottom: 5, fontSize: 12 }}>
                                        <a className="sign-form-forgot" href="">
                                            Forgot password
                                </a>
                                    </div>
                                    <Button type="primary" htmlType="submit" className="sign-form-button">
                                        Log in
                            </Button>
                                </Form>
                            </TabPane>
                            <TabPane tab="Sign Up" key="2">
                                <Title level={3}>Register</Title>
                                <Form onSubmit={this.handleSignUpSubmit} className="sign-form">
                                    <Form.Item>
                                        {getFieldDecorator('firstname', {
                                            rules: [
                                                {
                                                    required: true,
                                                    message: 'Please input your First name!'
                                                }
                                            ],
                                        })(
                                            <Input
                                                placeholder="First Name"
                                            />,
                                        )}
                                    </Form.Item>
                                    <Form.Item>
                                        {getFieldDecorator('email', {
                                            rules: [
                                                {
                                                    required: true,
                                                    message: 'Please input your email!'
                                                },
                                                {
                                                    type: 'email',
                                                    message: 'The input is not valid E-mail!',
                                                },
                                            ],
                                        })(
                                            <Input
                                                prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                                placeholder="Email"
                                            />,
                                        )}
                                    </Form.Item>
                                    <Form.Item>
                                        {getFieldDecorator('password', {
                                            rules: [{ required: true, message: 'Please input your Password!' }],
                                        })(
                                            <Input.Password
                                                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                                type="password"
                                                placeholder="Password"
                                            />,
                                        )}
                                    </Form.Item>

                                    <Button type="primary" htmlType="submit" className="sign-form-button">
                                        Sign Up!
                            </Button>
                                </Form>

                            </TabPane>
                        </Tabs>
                    </Card>
                </Content>
            </Layout>
        );
    };
};

const WrappedLoginForm = Form.create({ name: 'form_login' })(Login);

export default WrappedLoginForm;