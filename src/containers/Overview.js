import React from "react";
import { Row, Col, Typography, Card, Carousel } from 'antd';
import { CurrencyStatText } from '../components/CurrencyStatText';
import moment from 'moment';
import './Overview.css'
import { LastTransactionsList } from "../components/LastTransactionsList";
import { IncomeOutcome } from "../components/IncomeOutcome";
import { BalanceGraph } from "../components/BalanceGraph";
import { FiltersContainer } from "../components/FiltersContainer";
import { TopCategoryChart } from "../components/TopCategoryChart";


export class Overview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      totalBalance: 0.00,
      savings: 0.00,
      filteredCategories: [],
      startingCategories: [],
      checkBoxStartingCategories: [],
      filteredDates: { startDate: null, endDate: null },
      topCategoriesValues: [],
      filteredTransactions: [],
      isCategoryFilterActive: false,
      isDateFilterActive: false,
      activeDateFilter: 'alltime'
    };
    this.handleTransactionAdded = this.handleTransactionAdded.bind(this);
  }


  //component life
  componentDidUpdate(prevProps) {
    if (prevProps.accountList !== this.props.accountList) {
      this.retrieveOverviewValues();
    }

    if (prevProps.categories !== this.props.categories) {
      this.popolateStartingCategories();
    }

    if (prevProps.userCreationDate !== this.props.userCreationDate || prevProps.transactions !== this.props.transactions) {
      this.setState({
        filteredDates: { startDate: new moment(this.props.userCreationDate), endDate: new moment().add(10, 'days') },
      }, () => {
        this.filterTransactionsAllFilter();
        this.calcolateGlobalIncomeOutcome();
      })
    }


    if (prevProps.selectedAccount !== this.props.selectedAccount) {
      this.handleChangeOfAccount();
    }
  }

  componentDidMount() {
    this.filterTransactionsAllFilter();
    this.calcolateGlobalIncomeOutcome();
    this.retrieveOverviewValues();
  }

  componentWillUnmount() {
    this.handleRemoveCategoryFilter();
    this.handleRemoveDateFilter();
  }

  //account switched from sidebar
  handleChangeOfAccount = () => {
    this.setState({
      filteredDates: { startDate: moment().startOf('month'), endDate: moment().endOf('month') },
      isDateFilterActive: false
    }, () => {
      this.filterTransactionsAllFilter();
      this.calcolateGlobalIncomeOutcome();
      this.retrieveOverviewValues();
    })

  }

  //overview values
  calculateTotalBalance = () => {
    if (this.props.selectedAccount === 'overview') {
      let totalBalance = this.props.accountList.reduce((acc, account) => {
        return acc + account.balance
      }, 0);
      return totalBalance
    } else {
      return (this.props.accountList.find(({ id }) => id === this.props.selectedAccount)).balance
    }
  }

  retrieveOverviewValues = () => {
    //value of every account summed
    const totalBalance = this.calculateTotalBalance()
    this.setState({
      totalBalance: totalBalance,
    });
  }

  //transactions 
  handleTransactionAdded = (e, newTransaction) => {
    this.props.addTransaction(e, newTransaction);
    this.retrieveOverviewValues();
    if (newTransaction.source === "outcome" && this.checkOnFilteredDates(newTransaction) && this.checkOnFilteredAccounts(newTransaction)) {
      this.updateCategoriesChart(newTransaction)
    }
  };

  //filtering
  popolateStartingCategories = () => {
    let startingCategories = [];
    let checkBoxStartingCategories = [];

    this.props.categories.map(category => {
      //popolate category checkboxes
      let checkboxItem = new Object();
      checkboxItem.label = category.name;
      checkboxItem.value = category.id;
      checkboxItem.icon = category.icon;
      checkboxItem.color = category.color;
      checkBoxStartingCategories.push(checkboxItem);
      //popolate starting categories
      startingCategories.push(category.id);
    });

    this.setState({
      checkBoxStartingCategories: checkBoxStartingCategories,
      filteredCategories: startingCategories,
      startingCategories: startingCategories,
    })
  }

  handleUpdateCategoryFilter = filteredCategories => {
    let isCategoryFilterActive = true
    let updatedFilteredCategories = filteredCategories;
    if (!filteredCategories.length > 0) {
      isCategoryFilterActive = false;
      updatedFilteredCategories = this.state.startingCategories
    }
    this.setState({
      filteredCategories: updatedFilteredCategories,
      isCategoryFilterActive: isCategoryFilterActive
    }, () => {
      this.filterTransactionsAllFilter();
    })
  }

  handleUpdateDateFilter = (startDate, endDate, activeDateFilter) => {
    let newFilteredDateObj = new Object();
    let isDateFilterActive = false;
    newFilteredDateObj.startDate = startDate;
    newFilteredDateObj.endDate = endDate;
    if (activeDateFilter === 'custom' || activeDateFilter === 'week' || activeDateFilter === 'year' || activeDateFilter === 'month') {
      isDateFilterActive = true;
    }
    this.setState({
      filteredDates: newFilteredDateObj,
      isDateFilterActive: isDateFilterActive,
      activeDateFilter: activeDateFilter
    }, () => {
      this.filterTransactionsAllFilter();
    })
  }

  handleRemoveCategoryFilter = () => {
    this.popolateStartingCategories();
    this.setState({
      isCategoryFilterActive: false
    }, () => {
      this.filterTransactionsAllFilter();
    })
  }

  handleRemoveDateFilter = () => {
    this.setState({
      filteredDates: { startDate: new moment(this.props.userCreationDate), endDate: new moment().add(10, 'days') },
      isDateFilterActive: false
    }, () => {
      this.filterTransactionsAllFilter();
      this.calcolateGlobalIncomeOutcome();
    })
  }

  popolateCategoriesAmounts = () => {
    let topCategoriesValues = [];

    this.state.filteredTransactions.filter(transaction => transaction.amount < 0 /*aggiungere controllo per trasferimenti da account a account*/).reduce((res,transaction)=>{
      if(!res[transaction.category]){
        let matchedCategory = this.props.categories.find(category => category.id === transaction.category)
        res[transaction.category] = { id: transaction.category, amount: 0, color: matchedCategory.color, icon: matchedCategory.icon, name: matchedCategory.name}
        topCategoriesValues.push(res[transaction.category]);
      }

      res[transaction.category].amount += Math.abs(transaction.amount);
      return res
    }, {})

    const sortedTopCategories = topCategoriesValues.sort((a, b) => b.amount - a.amount);
    this.setState({
      topCategoriesValues: sortedTopCategories
    })
  }

  updateCategoriesChart = transaction => {
    let updatedTopCategories = this.state.topCategoriesValues;

    let matchedCategory = (updatedTopCategories.find((({ id }) => id === transaction.category)));
    matchedCategory.amount = matchedCategory.amount + transaction.amount;

    //sort 
    const sortedUpdatedTopCategories = updatedTopCategories.sort((a, b) => b.amount - a.amount);

    this.setState({
      topCategoriesValues: sortedUpdatedTopCategories
    });
  }

  //utils
  formatToEur = numberToBeFormatted => {
    const currencyFormatter = require('currency-formatter');
    return currencyFormatter.format(numberToBeFormatted, { code: 'EUR' })
  }

  unformatFromEur = numberToBeFormatted => {
    const currencyFormatter = require('currency-formatter');
    return currencyFormatter.unformat(numberToBeFormatted, { code: 'EUR' });
  }

  checkOnFilteredDates = transaction => {
    if (this.state.filteredDates.startDate === null && this.state.filteredDates.endDate === null) {
      return true
    } else {
      return (moment(transaction.date).isBetween(this.state.filteredDates.startDate, this.state.filteredDates.endDate, 'days', '[]'))
    }
  }

  checkOnFilteredCategories = transaction => {
    return this.state.filteredCategories.indexOf(transaction.category) > -1
  }

  checkOnFilteredAccounts = transaction => {

    if (this.props.selectedAccount !== 'overview') {
      return transaction.account === this.props.selectedAccount;
    } else {
      return true
    }
  }

  //get filter results
  filterTransactionsAllFilter = () => {
    let filteredTransactions = this.props.transactions.filter(transaction => (this.checkOnFilteredAccounts(transaction) && this.checkOnFilteredCategories(transaction) && this.checkOnFilteredDates(transaction))).sort((a, b) => new moment(b.date).format('YYYYMMDD') - new moment(a.date).format('YYYYMMDD'));
    this.setState({
      filteredTransactions: filteredTransactions
    }, () => {
      this.popolateCategoriesAmounts()
    })
  }

  //do not conisder category
  calcolateGlobalIncomeOutcome = () => {
    let incomeTransactions = this.props.transactions.filter(transaction => (this.checkOnFilteredAccounts(transaction) && transaction.amount > 0 && this.checkOnFilteredDates(transaction))) ;
    let outcomeTransactions = this.props.transactions.filter(transaction => (this.checkOnFilteredAccounts(transaction) && transaction.amount < 0 && this.checkOnFilteredDates(transaction))) ;
    
    const globalIncome = this.reduceTransactionList(incomeTransactions);
    const globalOutcome = this.reduceTransactionList(outcomeTransactions)

    this.setState({
      globalIncome : globalIncome,
      globalOutcome : globalOutcome 
    })
  }

  reduceTransactionList = transactionList => {
    return transactionList.reduce((acc, transaction) => {
      return acc + Math.abs(transaction.amount)
    },  0)  
  }

  //filter from top categories chart
  handleUpdateCategoryFilterTopCats = category => {
    if (this.state.filteredCategories !== this.state.startingCategories) {
      if (this.state.filteredCategories.indexOf(category.id) > -1) {
        this.removeCategoryFromFilters(category.id)
      } else {
        this.addCategoryToFilters(category.id);
      }
    } else {
      this.addFirstCategoryToFilters(category.id)
    }
  }

  addCategoryToFilters = (newelement) => {
    this.setState(prevState => ({
      filteredCategories: [...prevState.filteredCategories, newelement],
      isCategoryFilterActive: true
    }), () => {
      this.filterTransactionsAllFilter();
    })
  }

  addFirstCategoryToFilters = (newelement) => {
    this.setState({
      filteredCategories: []
    }, () => {
      this.addCategoryToFilters(newelement)
    })
  }

  resetFilteredTopCats = () => {
    this.setState({
      isCategoryFilterActive: false,
      filteredCategories: this.state.startingCategories
    }, () => {
      this.filterTransactionsAllFilter();
    })
  }

  removeCategoryFromFilters = (toBeRemoved) => {
    let updatedCategoryFilter = [...this.state.filteredCategories];
    updatedCategoryFilter.splice(updatedCategoryFilter.indexOf(toBeRemoved), 1);

    if (updatedCategoryFilter.length === 0) {
      this.resetFilteredTopCats();
    } else {
      this.setState({
        filteredCategories: updatedCategoryFilter
      }, () => {
        this.filterTransactionsAllFilter();
      })
    }
  }

  AccountName = () => {
    const { Title } = Typography;

    if (this.props.selectedAccount !== 'overview') {
      return <Title className="account-title">{this.props.accountList.find(({ id }) => id === this.props.selectedAccount).name}</Title>
    } else {
      return <Title className="account-title">Overview</Title>
    }
  }

  handleUpdateTransaction = (id, updatedTransaction) => {
    this.props.handleUpdateTransaction(id, updatedTransaction)
  }

  handleDeleteTransaction = (id, account, amount) => {
    this.props.handleDeleteTransaction(id, account, amount)
  }



  render() {

    const { Text } = Typography;



    return (

      <div style={{width: '100%'}}>
        <Row className="account-name-row" type="flex">
          <Col span={24}>
            <this.AccountName />
          </Col>
        </Row>
        <Row type="flex" className="main-cards-row" gutter={[{ md: 32, lg: 32 }, { xs: 0, sm: 24, md: 24, lg: 24, xl: 64 }]}>
          <Col xs={24} md={12}>
            <Card className="main-card" bordered={false}>
              <Text className="overview-title-text">Total Balance</Text>
              <CurrencyStatText titleLevel={1} statValue={this.formatToEur(this.state.totalBalance)} />
            </Card>
          </Col>
          <Col xs={24} md={12}>
            <IncomeOutcome
            globalIncome={this.formatToEur(this.state.globalIncome)}   
            globalOutcome={this.formatToEur(this.state.globalOutcome)}
            />
          </Col>
        </Row>
        <Row className="filter-row" gutter={[{ lg: 32 }, { xs: 0, sm: 24, md: 24, lg: 24, xl: 64 }]}>
          <Col>
            <Card className="standard-card" bordered={false}>
              <FiltersContainer
                userCreationDate={this.props.userCreationDate}
                removeCategoryFromFilters={category => this.removeCategoryFromFilters(category)}
                addFirstCategoryToFilters={category => this.addFirstCategoryToFilters(category)}
                addCategoryToFilters={category => this.addCategoryToFilters(category)}
                isCategoryFilterActive={this.state.isCategoryFilterActive}
                isDateFilterActive={this.state.isDateFilterActive}
                topCategoriesValues={this.state.topCategoriesValues}
                filteredDates={this.state.filteredDates}
                filteredCategories={this.state.filteredCategories}
                handleRemoveDateFilter={() => this.handleRemoveDateFilter()}
                handleUpdateDateFilter={(startDate, endDate, activeDateFilter) => this.handleUpdateDateFilter(startDate, endDate, activeDateFilter)}
                handleRemoveCategoryFilter={() => this.handleRemoveCategoryFilter()}
                handleUpdateCategoryFilter={filteredCategories => this.handleUpdateCategoryFilter(filteredCategories)}
                checkBoxStartingCategories={this.state.checkBoxStartingCategories}
              />
            </Card>
          </Col>
        </Row>
        <Row gutter={[{ lg: 32 }, { xs: 0, sm: 24, md: 24, lg: 24, xl: 64 }]}>
          <Col>
            <Card className="standard-card" bordered={false}>
              {window.matchMedia("(max-width: 1199.98px)").matches ? (
                <Carousel
                  dotPosition="top"
                  customPaging={i => {
                    return <div style={{ width: 30, cursor: 'pointer' }}>{
                      i !== 1 ? <i class='uil uil-chart-line' style={{ fontSize: 18, color: '#3B3B3B' }}></i> : <i class='uil uil-chart-bar' style={{ fontSize: 18, color: '#3B3B3B' }}></i>
                    }</div>
                  }}
                  appendDots={dots => {
                    return <div style={{ height: 50, top: -10, display: 'flex', justifyContent: 'flex-end', right: 26, alignItems: 'center' }}>
                      <ul style={{ margin: "0px" }}> {dots} </ul>
                    </div>
                  }}
                >
                  <BalanceGraph
                    formatToEur={(e) => this.formatToEur(e)}
                    filteredDates={this.state.filteredDates}
                    activeDateFilter={this.state.activeDateFilter}
                    filteredTransactions={this.state.filteredTransactions}
                  />


                  <TopCategoryChart
                    updateCategoryFilterTopCats={category => this.handleUpdateCategoryFilterTopCats(category)}
                    formatToEur={(e) => this.formatToEur(e)}
                    filteredCategories={this.state.filteredCategories}
                    data={this.state.topCategoriesValues}
                  />

                </Carousel>
              ) : <BalanceGraph
                  formatToEur={(e) => this.formatToEur(e)}
                  filteredDates={this.state.filteredDates}
                  activeDateFilter={this.state.activeDateFilter}
                  filteredTransactions={this.state.filteredTransactions}
                />
              }
            </Card>
          </Col>
        </Row>
        <Row type="flex" gutter={[{ lg: 32 },{ xs: 24, sm: 24, md: 24, lg: 24, xl: 64 }]}>
          <Col className="transactions-list" xs={{ span: 24, order: 2 }} xl={{ span: 14, order: 1 }}>
            <LastTransactionsList
            selectedAccount={this.props.selectedAccount}
              formatToEur={(e) => this.formatToEur(e)}
              handleTransactionAdded={(e, newTransaction) => this.handleTransactionAdded(e, newTransaction)}
              retrieveOverviewValues={() => this.retrieveOverviewValues()}
              handleDeleteTransaction={(id, account, amount) => this.handleDeleteTransaction(id, account, amount)}
              handleUpdateTransaction={(id, updatedTransaction) => this.handleUpdateTransaction(id, updatedTransaction)}
              categories={this.props.categories}
              accountList={this.props.accountList}
              filteredTransactions={this.state.filteredTransactions}
            />
          </Col>
          <Col xs={{ span: 24, order: 1 }} xl={{ span: 10, order: 2 }}>
          {window.matchMedia("(max-width: 1199.98px)").matches ? null : ( 
          <Card className="standard-card" bordered={false}>
          <TopCategoryChart
                    updateCategoryFilterTopCats={category => this.handleUpdateCategoryFilterTopCats(category)}
                    formatToEur={(e) => this.formatToEur(e)}
                    filteredCategories={this.state.filteredCategories}
                    data={this.state.topCategoriesValues}
            /> </Card> )}
          </Col>
        </Row>
      </div >
    );
  }
}

export default Overview;